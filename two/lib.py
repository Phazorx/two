import uuid
import json
import datetime
from pyramid.response import Response
from sqlalchemy.types import TypeDecorator, Binary
from sqlalchemy.dialects import postgresql
from pyramid.request import Request
from pyramid.decorator import reify


class TwoRequest(Request):
    @reify
    def id(self):
        return str(uuid.uuid4())

    def __init__(self, environ, **kw):
        self.now = datetime.datetime.utcnow()
        super(TwoRequest, self).__init__(environ, **kw)


class UUID(TypeDecorator):
    """
        Platform-independent GUID type.

        Uses Postgresql's UUID type, otherwise uses BINARY(16)

    """
    impl = Binary

    def load_dialect_impl(self, dialect=None):
        if dialect and dialect.name == 'postgresql':
            return dialect.type_descriptor(postgresql.UUID())
        else:
            return dialect.type_descriptor(Binary(16))

    def process_bind_param(self, value, dialect=None):  # python to query params
        if value is None:
            return value
        elif isinstance(value, uuid.UUID):
            if dialect and dialect.name == 'postgresql':
                return value
            else:
                return value.bytes
        elif isinstance(value, str):
            if dialect and dialect.name == 'postgresql':
                return uuid.UUID(value)
            else:
                return uuid.UUID(value).bytes
        else:
            raise ValueError('value %s is not a valid uuid.UUID' % value)

    def process_result_value(self, value, dialect=None):  # query result to python structure
        if value is None:
            return value
        if dialect and dialect.name == 'postgresql':
            return value
        elif isinstance(value, uuid.UUID):  # some drivers apparently return proper structure?
            return value
        else:
            return uuid.UUID(bytes=value)


def log(logger, request, response):
        rid = request.id
        finish = datetime.datetime.utcnow()
        start = request.now
        query = request.query_string
        method = request.method
        path = request.path
        code = response.status_int
        text = response.unicode_body
        size = len(text)
        time = (finish - start).total_seconds()
        timestamp = start.isoformat()
        logger.info('{} {} {} {} {} {} {} {}'.format(timestamp, code, rid, method, path, query, size, time))

class ErrorResponse(Response):
    def __init__(self, exc, status=500, *args, **kwargs):
        super(ErrorResponse, self).__init__(*args, **kwargs)
        self.status_int = status
        self.body = json.dumps(dict(errors=dict(code=exc.code, mesage=exc.message)))
        self.content_type = 'application/json'


class TwoException(Exception):
    def __init__(self, code, message):
        self.code = code
        self.message = message
        super(TwoException, self).__init__(message)
        # logging happens here?

