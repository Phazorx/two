import threading


def psycopg2_hacks():
    from psycopg2 import errorcodes, IntegrityError
    from psycopg2.extras import register_uuid
    from zope.sqlalchemy.datamanager import _retryable_errors

    # hack 1: UUID
    register_uuid()

    # hack 2: expand retry-able exceptions for transaction manager
    _retryable_errors.append((
        IntegrityError,
        lambda e: e.pgcode == errorcodes.UNIQUE_VIOLATION
    ))


def thread_name_tween_factory(handler, registry):
    """
    borrowed from
    http://www.alexconrad.org/2012/08/log-unique-request-ids-with-pyramid.html
    """
    def thread_name_tween(request):
        current_thread = threading.current_thread()
        original_name = current_thread.name

        # Hack in the request ID inside the thread's name, relies on id being request`s attribute
        current_thread.name = "%s|%s" % (original_name, getattr(request, 'id', None))
        try:
            response = handler(request)
        finally:
            # Restore the thread's original name when done
            current_thread.name = original_name
        return response
    return thread_name_tween