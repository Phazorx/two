import logging
from pyramid.view import view_config
from lib import TwoException, ErrorResponse, log
#import transaction

usage_logger = logging.getLogger('usage')
error_logger = logging.getLogger()


def success(event):
    # hack to avoid debug_toolbar requests in the log
    if '_debug_toolbar' in getattr(event.request, 'path'):
        return
    try:
        log(usage_logger, event.request, event.response)
    except Exception as e:
        error_logger.critical('! Could not log request handling: [%s]' % e)


def begin(event):
    # hack to avoid debug_toolbar requests in the log
    if '_debug_toolbar' in getattr(event.request, 'path'):
        return
#    transaction.begin()


@view_config(context=Exception)
def failure(exc, request):
#    transaction.abort()
    if isinstance(exc, TwoException):
        response = ErrorResponse(exc)
        return response
    else:
        raise exc
