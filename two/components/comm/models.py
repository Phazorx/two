from sqlalchemy import Column, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref
import datetime
import uuid
from ...data import Base
from ...lib import UUID


class Contact(Base):
    __tablename__ = 'contact'

    id = Column(UUID(), primary_key=True)
    value = Column(String(255))
    created_at = Column(DateTime, default=None)
    deleted_at = Column(DateTime, default=None)
    user_id = Column(UUID(), ForeignKey('user.id'))
    user = relationship("User", backref="contacts")
#    user = relationship("User", backref=backref("contacts", lazy='joined'), lazy='joined')

    def __init__(self, value=None):
        self.id = uuid.uuid4()
        self.value = value
        now = datetime.datetime.now()
        self.created_at = now

    def delete(self):
        now = datetime.datetime.now()
        self.deleted_at = now

    def is_active(self, now=None):
        if not now:
            now = datetime.datetime.now()
        return self.created_at < now and (self.deleted_at is None or self.deleted_at > now)

    def bind(self, user):
        if self.user is None:
            self.user = user
        else:
            raise Exception("the contact is already bound to another user")

    def unbind(self):
        if self.user is None:
            raise Exception("the contact is not bound to any user")
        else:
            self.user = None

    def __repr__(self):
        return '<Contact %r>' % self.value

    def __json__(self, r):
        return dict(
            id=str(self.id),
            value=self.value
        )
