from sqlalchemy import select, and_, or_
from ...lib import TwoException
from ...data import DBSession
from models import Contact

Contacts = DBSession.query(Contact)


def create_contact(value):
    contact = Contact(value=value)
    DBSession.add(contact)
    return contact


def bind_contact(contact, user):
    if contact.is_bound():
        raise TwoException(4, 'the contact is already bound')
    else:
        contact.bind(user)


def update_contact_value(contact, value):
    if value:
        contact.value = value
    else:
        raise TwoException(1, 'value must be set')


def unbind_contact(contact):
    if contact.is_bound():
        contact.unbind()
    else:
        raise TwoException(2, 'the contact is not bound to a user')


def delete_contact(contact):
    if contact.is_active():
        contact.delete()
    else:
        raise TwoException(3, 'the contact is already deleted')


def get_active_contacts_for_user_orm(user):
    active_contacts = list()
    for contact in Contacts.filter(Contact.user == user).all():
        if contact.is_active():
            active_contacts.append(contact)
    return Contacts.filter(Contact.deleted_at == None).all()


def get_active_contacts_for_user_core(user):
    import datetime
    table = Contact.__table__
    now = datetime.datetime.now()
    q = select([table], table.c.user_id == user.id).where(
        and_(
            table.c.created_at < now,
            or_(
                table.c.deleted_at is None, table.c.deleted_at > now
            )
        )
    )
    active_contacts = list()
    for r in DBSession.execute(q).fetchall():
        active_contacts.append(dict(r))
    return active_contacts


def get_contact_by_id(contact_id):
    return Contacts.get(contact_id)
