import logging
from ...lib import TwoException
from models import User
from ...data import DBSession


def _is_user_name_unique(name):
    return DBSession.query(User).filter(User.name == name).first() is None


def create_user(name):
    if _is_user_name_unique(name):
        user = User(name)
        DBSession.add(user)
        return user
    else:
        raise TwoException(1, 'duplicate user name')


def change_user_name(user, name):
    if _is_user_name_unique(name):
        user.name = name
    else:
        raise TwoException(2, 'user name must be unique')


def delete_user(user):
    logging.getLogger("auth.api").debug("deleting user")
    if user.is_deleted():
        raise TwoException(3, 'user is already deleted')
    else:
        user.delete()


def get_active_users():
    return DBSession.query(User).filter(User.deleted_at == None).all()


def get_user_by_id(user_id):
    return DBSession.query(User).get(user_id)
