from sqlalchemy import Column, String, DateTime
import datetime
import uuid
from ...data import Base
from ...lib import UUID


class User(Base):
    __tablename__ = 'user'

    id = Column(UUID(), primary_key=True)
    name = Column(String(50), unique=True)
    deleted_at = Column(DateTime, default=None)

    def __init__(self, name=None):
        self.id = uuid.uuid4()
        self.name = name

    def delete(self):
        now = datetime.datetime.now()
        self.deleted_at = now

    def is_deleted(self):
        return self.deleted_at is None

    def __repr__(self):
        return '<User %r>' % self.name

    def __json__(self, r):
        return dict(
            id=str(self.id),
            name=self.name
        )
