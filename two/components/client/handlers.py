from pyramid.view import view_config
import uuid
import api as client

@view_config(route_name='home', renderer='json')
def home(request):
    return {'version': 'prototype'}


@view_config(route_name='register', renderer='json')
def register(request):
    name = request.GET.getone('name')
    user = client.register(name)
    return user


@view_config(route_name='show_users', renderer='json')
def show_users(request):
    return client.list_active_users()


@view_config(route_name='delete', renderer='json')
def delete_user(request):
    user_id = request.GET.getone('user')
    user_uid = uuid.UUID(user_id)
    return client.delete_user(user_uid)


@view_config(route_name='rename', renderer='json')
def rename_user(request):
    user_id = request.GET.getone('user')
    user_uid = uuid.UUID(user_id)
    user_name = request.GET.getone('name')
    return client.rename_user(user_uid, user_name)


@view_config(route_name='register_contact', renderer='json')
def register_contact(request):
    user_id = request.GET.getone('user')
    user_uid = uuid.UUID(user_id)
    contact_value = request.GET.getone('value')
    return client.register_contact_for_user(user_uid, contact_value)


@view_config(route_name='remove_contact', renderer='json')
def remove_contact(request):
    contact_id = request.GET.getone('contact')
    contact_uid = uuid.UUID(contact_id)
    return client.remove_contact(contact_uid)


@view_config(route_name='show_contacts', renderer='json')
def show_contacts(request):
    user_id = request.GET.getone('user')
    user_uid = uuid.UUID(user_id)
    return client.list_active_contact_for_user(user_uid)
