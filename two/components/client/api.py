from ..auth import api as auth
from ..comm import api as comm


def register(name):
    user = auth.create_user(name)
    return user


def delete_user(user_id):
    user = auth.get_user_by_id(user_id)
    auth.delete_user(user)
    return True


def rename_user(user_id, name):
    user = auth.get_user_by_id(user_id)
    auth.change_user_name(user, name)
    return True


def list_active_users():
    output = list()
    for user in auth.get_active_users():
        # Todo: some projection/view magic here
        output.append(user)
    return output


def register_contact_for_user(user_id, contact_data):
    contact = comm.create_contact(contact_data)
    user = auth.get_user_by_id(user_id)
    comm.bind_contact(contact, user)
    return True


def remove_contact(contact_id):
    contact = comm.get_contact_by_id(contact_id)
    comm.delete_contact(contact)
    return True


def list_active_contact_for_user(user_id):
    user = auth.get_user_by_id(user_id)
    output = comm.get_active_contacts_for_user_orm(user)
    return output
