from pyramid.config import Configurator
from pyramid.events import NewResponse, NewRequest
from sqlalchemy import engine_from_config
from hacks import psycopg2_hacks, thread_name_tween_factory
from data import initdb
from lib import TwoRequest


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    if settings.get('sqlalchemy.url', False) and 'psycopg2' in settings['sqlalchemy.url']:
        psycopg2_hacks()

    engine = engine_from_config(settings, 'sqlalchemy.')
    initdb(engine)
    config = Configurator(settings=settings)
    config.set_request_factory('{0.__module__}.{0.__name__}'.format(TwoRequest))
    config.add_tween('{0.__module__}.{0.__name__}'.format(thread_name_tween_factory))
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('register', '/register')
    config.add_route('register_contact', '/register_contact')
    config.add_route('show_users', '/show_users')
    config.add_route('show_contacts', '/show_contacts')
    config.add_route('remove_contact', '/remove_contact')
    config.add_route('delete', '/delete')
    config.add_route('rename', '/rename')
#    config.scan('.models')    # importing models
    config.scan('.components.client.handlers')  # importing handles
    if settings.get('pyramid.includes', False) and 'pyramid_debugtoolbar' in settings['pyramid.includes']:
        config.scan('.handling_debug')
#        config.add_subscriber('.handling_debug.begin', NewRequest)
        config.add_subscriber('.handling_debug.success', NewResponse)
    else:
        config.scan('.handling')
#        config.add_subscriber('.handling.begin', NewRequest)
        config.add_subscriber('.handling.success', NewResponse)
    return config.make_wsgi_app()
